const fs = require('fs'),
    http = require('http'),
    path = require('path'),
    WebTorrent = require("WebTorrent"),
    client = new WebTorrent()

    // var magnetURI = 'magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent'

	function addTorrent(magnetURI) {
		const info = client.add(magnetURI, { path: __dirname }, function (torrent) {
		  torrent.on('done', function () {
			console.log('torrent download finished')
		  })
		})
		console.log(info);
	}

/**
 * Create a server that display files present in the folder  and allow to download them
 * efficiently with the resume and parallel download
 */
http.createServer(function(req, res) {
    // browser always ask for a favicon so just return nothing
    if("/favicon.ico" == req.url) {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.write("nope");
      res.end();
      return;
    }

    if(req.method == "POST"){
        let body = '';
        req.on('data', function (data) {
            body += data;
        });

        req.on('end', function() {
			const urls = body.split("\r\n").slice(3, -2);
			console.log(urls);
			urls.forEach(addTorrent);
        });
    }
    // display the list of files
    if(req.url == "/"){
      fs.readdir(__dirname, function(err, files) {
        let list = `<form>
			<textarea style="width:1000px" id="fileToDownload">magnet:?xt=urn:btih:D60795899F8488E7E489BA642DEFBCE1B23C9DA0&dn=Kingsman:+The+Secret+Service+(2014)+720p+BrRip+x264+-+YIFY&tr=udp://tracker.yify-torrents.com/announce&tr=udp://open.demonii.com:1337&tr=udp://exodus.desync.com:6969&tr=udp://tracker.istole.it:80&tr=udp://tracker.publicbt.com:80&tr=udp://tracker.openbittorrent.com:80&tr=udp://tracker.leechers-paradise.org:6969&tr=udp://9.rarbg.com:2710&tr=udp://p4p.arenabg.ch:1337&tr=udp://p4p.arenabg.com:1337&tr=udp://tracker.coppersurfer.tk:6969&tr=udp://tracker.zer0day.to:1337/announce&tr=udp://tracker.leechers-paradise.org:6969/announce&tr=udp://coppersurfer.tk:6969/announce</textarea><br>
			<script>
				function addLink(){
				const magnet = document.querySelector("#fileToDownload").value
				const fd = new FormData();
				fd.set("magnet", magnet);
				const request = new XMLHttpRequest();
				request.open("POST", "/");
				request.send(fd);
				}
			</script>
		</form>
		<button id="addLink" onclick="addLink()">Add the download list</button>
		<br>
		`;
        files.forEach(file => {
          list += `<a href="/${file}">${encodeURI(file)}</a><br>`;
        });
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(list);
        res.end();
      });
      return;
    }

    const file = path.resolve(__dirname + decodeURI(req.url));
    const range = req.headers.range
    // get the information about the file like the size 
    fs.stat(file, (err, stats)=>{
      const total = stats.size;
      // normal download, but give information about length if the client want to do a paralell download
      if(!range){
        res.writeHead(200, { 'Content-Length': total, 'Content-Type': 'video/mp4' });
         // let the client download
        fs.createReadStream(file).pipe(res);
        return;
      }

      // download only the wanted part of the file 
      const positions = range.replace(/bytes=/, '').split('-');
      const start = parseInt(positions[0], 10);
      const end = positions[1] ? parseInt(positions[1], 10) : total - 1;
      res.writeHead(206, {
        'Content-Range': 'bytes ' + start + '-' + end + '/' + total,
        'Accept-Ranges': 'bytes',
        'Content-Length': (end - start) + 1,
        'Content-Type': 'video/mp4'
      });
      // let the client download
      fs.createReadStream(file, {start: start, end: end}).pipe(res);
    });    
}).listen(3001);